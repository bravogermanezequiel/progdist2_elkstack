FROM docker.elastic.co/beats/metricbeat:7.15.0
COPY ./metricbeat.yml /usr/share/metricbeat/metricbeat.yml
